.. GP at Home Docs documentation master file, created by
   sphinx-quickstart on Tue Sep  2 19:31:03 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GP at Home Docs's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   updating_pages
   activating_users
   user_invitation_and_payments
   test_user
   backup_policy
   attachments
