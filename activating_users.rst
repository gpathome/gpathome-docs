Activating / Deactivating users
===============================

This covers archiving users too.

To do this, login to the control panel and navigate to the **Users** screen.

Find the user you want to deactivate (archive) and click on **Deactivate**.

To reverse this click on **All Users** which is listed about the list of users (one of the tabs). Find the user then click on **Activate** on the right of the line.
