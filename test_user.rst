Testing with a test user
========================

Firstly you need to invite yourself on a different email address than the one you currently use. Within the control panel click on **Users** then on **Invite User**.

Fill out the required details and click on **Invite User**.

The crucial thing to note at this point is to make sure you log out. If you don't log out, the link in the email that you receive will **not** work correctly and the site will come up with a yellow notice box saying "Already logged in".

.. image:: already-logged-in.png

After clicking on the **Accept Invitation** link in the email you'll see a screen similar to the one below:

.. image:: password-reset.png

Once you login you will **not** be able to post a new consultation. You'll see a message:

.. image:: message-on-login.png

Click on the "Please click here" within the message.

Make sure you click on the checkbox next to "The information provided is correct to the best of my knowledge" at the bottom of the page and click on **Update Medical Profile**. You should see a message saying "Medical profile updated". Click on **Dashboard**. You will now see the **New Consultation** button.

Click on **New Consultation**.

Once you fill out **Subject** and **Message** click on **Send**.

You should then see a message saying "Consultation Created".

At this point, you can either log out of this account and log back in as the admin user or use a different browser.

Either way, when you log in to the admin account you should see a new consultation.

Clicking on the **Subject** of the consultation or the **Reply** link on that row will enable you to reply to the consultation.