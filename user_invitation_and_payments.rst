User invitations & Payments
===========================

Invitations
-----------

When a user visits the site then click on *Sign up* or *Membership Plans*. This then sends them to the plans page.

They then can click on Request Invite. This window then appears:

.. image:: request-invite.png

Once they fill this in, your practice's email address, configured when you setup GP at Home, will receive an email which states the plan the user requested along with their email address, first name and last name.

To invite this user you must then go to the control panel. Click on *Users* then on *Invite User* (top right of the screen). You will then see this screen:

.. image:: invitation-screen.png

Here you can set the plan type, the email (matching the email that was sent to your practice's email address), the first and last names.

Once ready, click on *Invite User*. This will send the user an email along with a link to set their password etc. Once the user resets their password they can start adding details to their medical profile etc and submit consultations.

Payments
--------

GP at Home also has the facility to track payments made by patients if needed.

On the *Users* screen you will also see a *Payments* link by the side of Account Holders. When you click on this link you will see the following screen:

.. image:: payments-screen.png

I have included some test payments on this screen as examples. Please note that the *Amount* does **not** need the pound sign. Just the value is fine.
