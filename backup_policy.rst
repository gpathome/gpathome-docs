Backup policy
=============

Definitions
-----------

**The Software** - An instance of GP at Home software.

**Backup** - An encrypted backup of the data from The Software (this includes assets and the database for individual instances of The Software.

Overview, Purpose and Scope
---------------------------

This policy defines the backup policy for the The Software which is expected to be backed up.

This policy is designed to protect data in The Software, to be sure it is recoverable in the event of equipment failure, intentional distruction of data or disaster.

This policy applies to instances (running copies of) The Software which includes assets (uploads within The Software) and the database from individual instances.

Timing
------

- A full Backup is to be taken once a week and stored off-site (Located within the UK, Backups to be sent to off-site location via secure link).

- Nightly Backups are to be taken and will rotate on a weekly basis (rolling 7 day period). These will be located on the server running The Software.

All backups are to be taken during a period of the day that the system is under the least amount of load to avoid any disruption.

Data Backed Up
--------------

- Assets - uploads through The Software.
- Database - the database of individual instances of The Software.

Restoration

In the event of a restore being needed a request must be made to restore@gpathome.com. Include the date you wish to restore prior to.
