Updating Pages
==============

To do this, login to the control panel and navigate to the **Pages** screen.

Within this screen pages can be *disabled*, *enabled* and *edited*.

Here is an overview of the pages screen:

.. image:: pages-screen.png


Editing
-------

Find the page you would like to edit and click on **Edit**.

There's a small toolbar in here for performing various formatting options.

The actual text is in a format called Markdown. You can read more about `Markdown here <https://daringfireball.net/projects/markdown/>`_.


Disabling/Enabling
------------------

To disable / enable a page, first find the page then click **Disable**.

To enable again, simply click **Enable**. You'll see a notice at the top of the screen in green.

There's a column (**Visible**) to show you the pages visibility on the main pages overview screen.


Deleting pages
--------------

Pages can not currently be deleted.