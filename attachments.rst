Attachments
===========

Sometimes you would like to attach something to show it to a user. This can be achieved
by replying to a consultation in the Admin control panel.

When you are on the reply screen you will see a sidebar to the right of the screen
with the folllowing:

.. image:: file-attachments.png

Add the attachments you wish to show the user here.

Once you have uploaded all the attachments reply to the consultation as usual.

The user will then receive a notification email saying that there has been a reply
to their consultation.

Once they login to their control panel and visit the consultation they will find
the following to the right of the main consultation screen:

.. image:: gp-attachments.png

